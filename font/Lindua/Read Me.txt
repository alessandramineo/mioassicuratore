Thank you for your purchase. We hope you enjoy using this icon pack.

All icons in this package have been designed on a 24x24 grid and look best when used at sizes that are a multiple of 24px. For example, 24px, 48px and 72px.

The icons come in different formats. To access all icons in a single file, you can use the PSD, PDF or the AI (Adobe Illustrator) file.
This package comes with a desktop font which you can install and use in any application that allows choosing custom fonts for your type. Refer to the "Read Me.txt" file inside the "Fonts" directory to learn more.
To access the icons via the IcoMoon app (https://icomoon.io/app), you can import the "Lindua.icomoon.json" file. Using this app, you can make custom webfonts or generate SVG sprites.
To access the icons as separate files, look under the "SVG" directory. The "PNG" folder contains all icons as raster images, in 4 different sizes.
The CSH file included in this pack is a Photoshop Custom Shapes file. By opening this file, all icons will get added to Photoshop, which can then be accessed via the Custom Shape Tool. You may need to restart Photoshop after opening the CSH file.

Follow us on Twitter @_vect_ (https://twitter.com/_vect_) to receive updates.
If you have any questions or suggestions regarding this package, feel free to email support@icomoon.io
